package com.sevenpeakssoftware.pragneshghoda.cars.feed.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.sevenpeakssoftware.pragneshghoda.cars.R;
import com.sevenpeakssoftware.pragneshghoda.cars.databinding.ActivityCarDetailsMainBinding;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.models.Article;

public class CarsDetailsActivity extends AppCompatActivity {

    private ActivityCarDetailsMainBinding carDetailsMainBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carDetailsMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_car_details_main);
        init();
    }

    private void init() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (getIntent().hasExtra(getString(R.string.car_model))) {
            Article itemDetail = (Article) getIntent().getSerializableExtra(getString(R.string.car_model));
            if (itemDetail != null) {
                carDetailsMainBinding.setArticleItem(itemDetail);
                carDetailsMainBinding.executePendingBindings();
            }
        }
    }
}
