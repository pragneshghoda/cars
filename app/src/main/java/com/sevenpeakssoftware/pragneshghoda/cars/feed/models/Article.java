
package com.sevenpeakssoftware.pragneshghoda.cars.feed.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Article implements Serializable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("dateTime")
    @Expose
    private String dateTime;
    @SerializedName("tags")
    @Expose
    private List<Object> tags = null;
    @SerializedName("content")
    @Expose
    private List<Content_> content = null;
    @SerializedName("ingress")
    @Expose
    private String ingress;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created")
    @Expose
    private Integer created;
    @SerializedName("changed")
    @Expose
    private Integer changed;
    private final static long serialVersionUID = -427351712029747134L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public List<Content_> getContent() {
        return content;
    }

    public void setContent(List<Content_> content) {
        this.content = content;
    }

    public String getIngress() {
        return ingress;
    }

    public void setIngress(String ingress) {
        this.ingress = ingress;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getCreated() {
        return created;
    }

    public void setCreated(Integer created) {
        this.created = created;
    }

    public Integer getChanged() {
        return changed;
    }

    public void setChanged(Integer changed) {
        this.changed = changed;
    }

}
