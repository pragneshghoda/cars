package com.sevenpeakssoftware.pragneshghoda.cars.databinding;

import android.content.Context;
import android.view.View;

import com.sevenpeakssoftware.pragneshghoda.cars.MainActivity;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.models.Article;

/**
 * Created by Pragnesh Ghoda.
 */

public class OnClickHandler {

    public void onFeedItemClick(Context context, View view, Article itemModel) {
        if (context instanceof MainActivity) {
            ((MainActivity) context).onItemClick(view, itemModel);
        }
    }
}