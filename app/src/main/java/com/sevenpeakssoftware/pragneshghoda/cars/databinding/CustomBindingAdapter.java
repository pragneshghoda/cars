package com.sevenpeakssoftware.pragneshghoda.cars.databinding;


import android.databinding.BindingAdapter;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by Pragnesh Ghoda.
 */

public class CustomBindingAdapter {

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, String url) {
        if (TextUtils.isEmpty(url)) return;
        Glide.with(imageView.getContext())
                .load(url)
                .into(imageView);
    }
}
