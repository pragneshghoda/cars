package com.sevenpeakssoftware.pragneshghoda.cars.webservice;

import com.sevenpeakssoftware.pragneshghoda.cars.feed.models.ArticlesListModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Pragnesh Ghoda.
 */
public interface AppService {

    @GET("article/get_articles_list")
    Call<ArticlesListModel> getArticleList();


}
