package com.sevenpeakssoftware.pragneshghoda.cars;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.sevenpeakssoftware.pragneshghoda.cars.databinding.ActivityMainBinding;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.CarsFeedFragment;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.details.CarsDetailsActivity;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.models.Article;
import com.sevenpeakssoftware.pragneshghoda.cars.other.OtherFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ActivityMainBinding activityMainBinding;
    private CarsFeedFragment carsFeedFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(activityMainBinding.appBar.toolbar);

        init();
    }

    @Override
    public void onBackPressed() {
        if (isSlidingMenuOpen())
            toggleSlidingMenu();
        else {
            super.onBackPressed();
        }
    }

    private void init(){
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, activityMainBinding.drawerLayout, activityMainBinding.appBar.toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        activityMainBinding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        activityMainBinding.navView.setNavigationItemSelectedListener(this);

        onNavigationItemSelected(activityMainBinding.navView.getMenu().getItem(0));
        activityMainBinding.navView.setCheckedItem(R.id.nav_cars);
    }

    private void setFragment(Fragment fragment) {

        try {
            FragmentManager frgManager = getSupportFragmentManager();
            FragmentTransaction ft = frgManager.beginTransaction();
            ft.replace(R.id.frameContainer, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();

        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void onItemClick(View view, Article itemModel) {
        Intent intentDetail = new Intent(this, CarsDetailsActivity.class);
        intentDetail.putExtra(getString(R.string.car_model), itemModel);
        startActivity(intentDetail);
    }

    private void toggleSlidingMenu() {
        if (isSlidingMenuOpen()) {
            activityMainBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            activityMainBinding.drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    private boolean isSlidingMenuOpen() {
        return activityMainBinding.drawerLayout != null && activityMainBinding.drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_cars) {
            if(carsFeedFragment == null)
                carsFeedFragment = new CarsFeedFragment();
            setFragment(carsFeedFragment);
        } else if (id == R.id.nav_other) {
            setFragment(new OtherFragment());
        }
        activityMainBinding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
