package com.sevenpeakssoftware.pragneshghoda.cars.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;
import android.view.WindowManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static void showAlertInfo(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static ProgressDialog createProgressDialog(Context mContext, String msg, boolean showLargeSizeProgress) {
        ProgressDialog dialog;
        dialog = new ProgressDialog(mContext);
        try {
        if (!TextUtils.isEmpty(msg))
            dialog.setMessage(msg);
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();

        }
        dialog.setCancelable(false);
        return dialog;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }



    /**
     * Serialize any object into String     *     * @param object Object     * @return String     * @throws IOException If unable to access Stream
     */
    public static String serialize(Object object) throws java.io.IOException {
        if (object == null) return null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new Base64OutputStream(byteArrayOutputStream, 0));
        objectOutputStream.writeObject(object);
        objectOutputStream.flush();
        objectOutputStream.close();
        return byteArrayOutputStream.toString();
    }

    /**
     * Deserialize provided string into Object     *     * @param serialized String     * @return Object     * @throws IOException            If unable to access Stream     * @throws ClassNotFoundException If unable to find class
     */
    public static Object deserialize(String serialized) throws Exception {
        if (serialized == null) return null;
        return new ObjectInputStream(new Base64InputStream(new ByteArrayInputStream(serialized.getBytes()), 0)).readObject();
    }


    public static String mGetFormattedDate(String date) {
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
            Date inputDate = inputFormat.parse(date);

            Calendar inputDateInstance = Calendar.getInstance();
            inputDateInstance.setTime(inputDate);

            String outputPattern = (inputDateInstance.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) ? "dd MMMM, HH:mm" :"dd MMMM yyyy, HH:mm";;

            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());
            return outputFormat.format(inputDate);
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
        return "";
    }
}