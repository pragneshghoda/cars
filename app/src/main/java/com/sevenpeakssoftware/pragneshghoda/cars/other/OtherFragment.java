package com.sevenpeakssoftware.pragneshghoda.cars.other;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sevenpeakssoftware.pragneshghoda.cars.R;
import com.sevenpeakssoftware.pragneshghoda.cars.databinding.FragmentOtherMainBinding;

public class OtherFragment extends Fragment {

    private FragmentOtherMainBinding otherMainBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        otherMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_other_main, container, false);
        return otherMainBinding.getRoot();
    }
}
