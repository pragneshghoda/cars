package com.sevenpeakssoftware.pragneshghoda.cars.feed;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sevenpeakssoftware.pragneshghoda.cars.R;
import com.sevenpeakssoftware.pragneshghoda.cars.databinding.OnClickHandler;
import com.sevenpeakssoftware.pragneshghoda.cars.databinding.RowCarsListItemBinding;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.models.Article;

import java.util.List;

public class CarsFeedAdapter extends RecyclerView.Adapter<CarsFeedAdapter.ItemViewHolder> {

    private List<Article> articlesList;

    public CarsFeedAdapter(List<Article> list) {
        this.articlesList = list;
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.row_cars_list_item, parent, false);
        viewHolder = new ItemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.getBinding().setArticleItem(articlesList.get(position));
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return articlesList == null ? 0 : articlesList.size();
    }


    public interface OnItemClickListener {
        void onItemClick(View view, Object item);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private RowCarsListItemBinding itemBinding;

        private ItemViewHolder(final View itemView) {
            super(itemView);
            itemBinding = DataBindingUtil.bind(itemView);
            itemBinding.setClickHandler(new OnClickHandler());
        }

        private RowCarsListItemBinding getBinding() {
            return itemBinding;
        }
    }
}
