package com.sevenpeakssoftware.pragneshghoda.cars;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import com.sevenpeakssoftware.pragneshghoda.cars.utils.Utils;
import com.sevenpeakssoftware.pragneshghoda.cars.webservice.RestClient;


/**
 * Created by Pragnesh Ghoda.
 */
public class CarsApplication extends Application {

    public static CarsApplication application;
    private SharedPreferences sharedPref;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        new RestClient(this);

        // shared preferences initial setup
        sharedPref = getSharedPreferences(
                getString(R.string.app_name), Context.MODE_PRIVATE);


        IntentFilter filter = new IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION);
    }

    public void putSingleValuePreference(String key, Object value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        if (value instanceof String)
            editor.putString(key, (String) value);
        else if (value instanceof Boolean)
            editor.putBoolean(key, (Boolean) value);
        else if (value instanceof Integer)
            editor.putInt(key, (Integer) value);
        else if (value instanceof Float)
            editor.putFloat(key, (Float) value);
        else if (value instanceof Long)
            editor.putLong(key, (Long) value);
        editor.apply();
    }

    public void removeSingleValuePreference(String key) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);
        editor.apply();
    }

    public String getPreferencesString(String key) {
        return sharedPref.getString(key, "");
    }


    public String getPreferencesStringDefaultNull(String key) {
        return sharedPref.getString(key, null);
    }

}