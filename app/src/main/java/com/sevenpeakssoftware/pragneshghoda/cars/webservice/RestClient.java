package com.sevenpeakssoftware.pragneshghoda.cars.webservice;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sevenpeakssoftware.pragneshghoda.cars.BuildConfig;
import com.sevenpeakssoftware.pragneshghoda.cars.R;
import com.sevenpeakssoftware.pragneshghoda.cars.utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestClient {
    private static final String langKey = "lang-code";
    static ProgressDialog progressDialog;

    private static AppService REST_CLIENT;
    private static Context context;

    public RestClient(Context context) {
        this.context = context;
        setupRestClient(context);
    }

    public static AppService getRestClient() {
        return REST_CLIENT;
    }

    public static void setupRestClient(final Context context) {

        OkHttpClient.Builder builderOkHttp = new OkHttpClient.Builder();
        builderOkHttp.connectTimeout(100, TimeUnit.SECONDS);
        builderOkHttp.readTimeout(100, TimeUnit.SECONDS);

        /*Comment below 3 lines to prevent RestClient to print body of all request and response */
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builderOkHttp.addInterceptor(httpLoggingInterceptor);

        builderOkHttp.addNetworkInterceptor(provideCacheInterceptor());
        builderOkHttp.cache(provideCache());

        OkHttpClient client = builderOkHttp.build();
        Gson gson = new GsonBuilder().setLenient()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit builder = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        REST_CLIENT = builder.create(AppService.class);
    }

    public static Interceptor provideCacheInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                request = request.newBuilder()
                        .addHeader("Accept", "*/*")
                        .build();

                Response originalResponse = chain.proceed(request);
                return originalResponse.newBuilder()
                        .build();
            }
        };
    }

    public static void makeApiRequest(final Activity activity, Object call, final ApiResponseListener apiResponseListener, final int reqCode, final boolean showProgressDialog) {
        try {
            Call<Object> objectCall = (Call<Object>) call;


            //if (Utils.isNetworkAvailable(activity)) {
            if (showProgressDialog)
                mDisplayProgressDialog(true, activity);

            objectCall.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, retrofit2.Response<Object> response) {
                    try {
                        if (showProgressDialog)
                            mDisplayProgressDialog(false, activity);

                        if (response.code() == 200) {

                            if (apiResponseListener != null) {
                                apiResponseListener.onApiResponse(call, response.body(), reqCode);
                            }

                        } else if (response.code() == 400) {
                            if (response.errorBody() != null) {
                                showDialog(activity, "Could not load data. Please try later.");
                            }
                        } else {
                            if (response.errorBody() != null) {
                                showDialog(activity, "Could not load data. Please try later.");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    //There is more than just a failing request (like: no internet connection)
                    Log.e("error", "" + t.toString());

                    if (showProgressDialog)
                        mDisplayProgressDialog(false, activity);

                    mDisplayProgressDialog(false, activity);
                    if (t instanceof TimeoutException || t instanceof SocketTimeoutException)
                        showDialog(activity, "connection_timeout");
                    else
                        showDialog(activity, "something_went_wrong");
                }
            });
         /*   } else {
                Utils.showDialog(activity, activity.getString(R.string.net_msg));
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //Converting Request body to string params
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static String getRequestStringBody(Request requestBody) {
        try (Buffer buffer = new Buffer()) {
            if (requestBody.body() != null) {
                requestBody.body().writeTo(buffer);
                return buffer.readUtf8();
            }
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * @param show     to show or hide progress dialog
     * @param activity activity context
     */
    public static void mDisplayProgressDialog(boolean show, Activity activity) {
        if (activity == null || activity.isFinishing())
            return;
        if (show) {
            if (progressDialog == null) {
                progressDialog = Utils.createProgressDialog(activity, activity.getString(R.string.loading), false);
                progressDialog.show();
            } else {
                progressDialog.show();
            }
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    /**
     * Showing error dialog when api response code is not 200(success)
     *
     * @param activity     - activity context
     * @param errorMessage - error message to show
     */
    private static void showDialog(Activity activity, String errorMessage) {
        if (TextUtils.isEmpty(errorMessage)) {
            Utils.showAlertInfo(activity, errorMessage);
        }
    }

    private static Cache provideCache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(context.getCacheDir(), "http-cache"),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {//Log.e( e, "Could not create Cache!" );
        }
        return cache;
    }

}