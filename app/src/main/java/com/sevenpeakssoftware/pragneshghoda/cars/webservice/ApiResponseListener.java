package com.sevenpeakssoftware.pragneshghoda.cars.webservice;

import retrofit2.Call;

/**
 * Created by pragnesh ghoda
 */
public interface ApiResponseListener {
    /**
     * Call on success API response with request code
     */
    void onApiResponse(Call<Object> call, Object response, int reqCode) throws Exception;
}
