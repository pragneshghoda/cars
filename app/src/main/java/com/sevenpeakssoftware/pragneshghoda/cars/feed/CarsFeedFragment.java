package com.sevenpeakssoftware.pragneshghoda.cars.feed;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sevenpeakssoftware.pragneshghoda.cars.CarsApplication;
import com.sevenpeakssoftware.pragneshghoda.cars.R;
import com.sevenpeakssoftware.pragneshghoda.cars.databinding.FragmentCarsListBinding;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.models.Article;
import com.sevenpeakssoftware.pragneshghoda.cars.feed.models.ArticlesListModel;
import com.sevenpeakssoftware.pragneshghoda.cars.utils.Utils;
import com.sevenpeakssoftware.pragneshghoda.cars.webservice.ApiResponseListener;
import com.sevenpeakssoftware.pragneshghoda.cars.webservice.RestClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;

public class CarsFeedFragment extends Fragment implements ApiResponseListener {

    private FragmentCarsListBinding carsListBinding;
    private List<Article> articleArrayList = new ArrayList<>();
    private final int CALL_ARTICLE_LIST_CODE = 1001;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        carsListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cars_list, container, false);
        return carsListBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        carsListBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                apiCallArticleList(false);
            }
        });
        apiCallArticleList(articleArrayList.isEmpty());
    }

    private void setAdapter() {
        CarsFeedAdapter adapter = new CarsFeedAdapter(articleArrayList);
        carsListBinding.rvCarsFeed.setAdapter(adapter);
        carsListBinding.rvCarsFeed.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


    private void apiCallArticleList(boolean showProgress) {
        if (Utils.isNetworkAvailable(carsListBinding.getRoot().getContext())) {
            Call objectCall = RestClient.getRestClient().getArticleList();
            RestClient.makeApiRequest(getActivity(), objectCall, this, CALL_ARTICLE_LIST_CODE, showProgress);
        } else {
            //getting serialized data from preference when device is offline.
            String str_offline_data = ((CarsApplication) carsListBinding.getRoot().getContext().getApplicationContext()).getPreferencesString(getString(R.string.latest_response));
            try {
                ArticlesListModel offline_data_model = (ArticlesListModel) Utils.deserialize(str_offline_data);
                articleArrayList = offline_data_model.getArticle();
                setAdapter();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(str_offline_data))
                Utils.showAlertInfo(getActivity(), getString(R.string.no_network_available));

            carsListBinding.swipeRefresh.setRefreshing(false);
        }

    }

    @Override
    public void onApiResponse(Call<Object> call, Object response, int reqCode) throws Exception {
        carsListBinding.swipeRefresh.setRefreshing(false);
        if (response != null) {
            if (reqCode == CALL_ARTICLE_LIST_CODE) {
                ArticlesListModel responseModel = (ArticlesListModel) response;
                if (responseModel.getStatus().equals("success")) {
                    //Setting data in preference to get when device is offline. Storing as serialized string of array list
                    ((CarsApplication) carsListBinding.getRoot().getContext().getApplicationContext()).putSingleValuePreference(getString(R.string.latest_response), Utils.serialize(responseModel));
                    articleArrayList = responseModel.getArticle();
                    setAdapter();
                } else {
                    Utils.showAlertInfo(getActivity(), getString(R.string.could_not_load_data));
                }
            }
        }
    }

}
